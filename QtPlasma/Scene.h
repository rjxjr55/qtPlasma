#pragma once
#include <vector>

class IDrawable;

class Scene
{
private:
	std::vector<IDrawable*> m_drawList;

private:
	Scene() {};
	static Scene instance;

public:
	virtual ~Scene();
	static Scene& GetInstance();

	void AddDrawable(IDrawable* item);

	void Update(float dt);
	void Draw(float dt);

	void Empty();
};

