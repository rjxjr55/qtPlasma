#pragma once
#include <vector>

#include "types.h"

class DataSource;

class Importer
{
private:
	std::vector<DataSource*> m_sources;
	QTreeWidget* m_treeDataSoure;

private:
	Importer() {};
	static Importer instance;

public:
	virtual ~Importer();
	static Importer& GetInstance();

	void Empty();

	void Load(std::string file, DataType type);
	void SetDataSourceTree(QTreeWidget* tree);

	std::vector<DataSource*>& GetDataSources();
private:
};

