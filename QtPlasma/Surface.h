#pragma once
#include "IDrawable.h"
#include <vector>
#include "types.h"

class Surface :
	public IDrawable
{
private:
	std::vector<Vector3f>* m_position;
	std::vector<int>* m_index;
	std::vector<Vector3f>* m_normal;
	std::vector<Vector3f>* m_color;

public:
	Surface(std::vector<Vector3f>* position);
	Surface();
	virtual ~Surface();

	std::vector<Vector3f>& GetPosition();
	std::vector<int>& GetIndex();
	std::vector<Vector3f>& GetNormal();
	std::vector<Vector3f>& GetColor();

	// Implement virtual methods
	virtual void Update(float dt) {};
	virtual void Draw(float dt);
};

