#ifndef __TYPES_H
#define __TYPES_H

typedef char int8;
typedef signed char sint8;
typedef unsigned char uint8;

typedef short int16;
typedef signed short sint16;
typedef unsigned short uint16;

typedef int int32;
typedef signed int sint32;
typedef unsigned int uint32;

typedef unsigned char byte;
typedef unsigned char BYTE;

typedef unsigned short WORD;
typedef unsigned long DWORD;

typedef struct {
	float x;
	float y;
	float z;
} vertex, Vector3f;

typedef struct {
	int x;
	int y;
	int z;
} Vector3i;

typedef enum {
	HDF,
	RAW,
} DataType;

#ifndef NULL
#define NULL 0
#endif

#define MAX_STR_LEN 1024


#endif //__TYPES_H