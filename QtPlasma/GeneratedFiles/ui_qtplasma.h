/********************************************************************************
** Form generated from reading UI file 'qtplasma.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTPLASMA_H
#define UI_QTPLASMA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glviewport.h"

QT_BEGIN_NAMESPACE

class Ui_QtPlasmaClass
{
public:
    QAction *actionOpen;
    QAction *actionWireframe;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    GLViewport *openGLWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuView;
    QDockWidget *dockScene;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTreeWidget *treeDataSource;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QPushButton *btnApply;
    QTreeWidget *treeRenderContext;
    QFrame *line;
    QGroupBox *horizontalGroupBox;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnVolume;
    QPushButton *btnEField;
    QPushButton *btnIsosurface;
    QPushButton *btnAlphaShape;
    QDockWidget *dockDetail;
    QWidget *dockWidgetContents_2;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_4;
    QTableWidget *tableWidget;

    void setupUi(QMainWindow *QtPlasmaClass)
    {
        if (QtPlasmaClass->objectName().isEmpty())
            QtPlasmaClass->setObjectName(QStringLiteral("QtPlasmaClass"));
        QtPlasmaClass->resize(1105, 838);
        actionOpen = new QAction(QtPlasmaClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionWireframe = new QAction(QtPlasmaClass);
        actionWireframe->setObjectName(QStringLiteral("actionWireframe"));
        actionWireframe->setCheckable(true);
        centralWidget = new QWidget(QtPlasmaClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        openGLWidget = new GLViewport(centralWidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));

        horizontalLayout->addWidget(openGLWidget);

        QtPlasmaClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QtPlasmaClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1105, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        QtPlasmaClass->setMenuBar(menuBar);
        dockScene = new QDockWidget(QtPlasmaClass);
        dockScene->setObjectName(QStringLiteral("dockScene"));
        dockScene->setFeatures(QDockWidget::DockWidgetFloatable|QDockWidget::DockWidgetMovable);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout = new QVBoxLayout(dockWidgetContents);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(dockWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        treeDataSource = new QTreeWidget(groupBox);
        treeDataSource->setObjectName(QStringLiteral("treeDataSource"));

        gridLayout->addWidget(treeDataSource, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(dockWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        btnApply = new QPushButton(groupBox_2);
        btnApply->setObjectName(QStringLiteral("btnApply"));

        gridLayout_2->addWidget(btnApply, 6, 0, 1, 1);

        treeRenderContext = new QTreeWidget(groupBox_2);
        treeRenderContext->setObjectName(QStringLiteral("treeRenderContext"));

        gridLayout_2->addWidget(treeRenderContext, 5, 0, 1, 1);

        line = new QFrame(groupBox_2);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 2, 0, 1, 1);

        horizontalGroupBox = new QGroupBox(groupBox_2);
        horizontalGroupBox->setObjectName(QStringLiteral("horizontalGroupBox"));
        horizontalGroupBox->setFlat(false);
        horizontalLayout_2 = new QHBoxLayout(horizontalGroupBox);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        btnVolume = new QPushButton(horizontalGroupBox);
        btnVolume->setObjectName(QStringLiteral("btnVolume"));

        horizontalLayout_2->addWidget(btnVolume);

        btnEField = new QPushButton(horizontalGroupBox);
        btnEField->setObjectName(QStringLiteral("btnEField"));

        horizontalLayout_2->addWidget(btnEField);

        btnIsosurface = new QPushButton(horizontalGroupBox);
        btnIsosurface->setObjectName(QStringLiteral("btnIsosurface"));

        horizontalLayout_2->addWidget(btnIsosurface);

        btnAlphaShape = new QPushButton(horizontalGroupBox);
        btnAlphaShape->setObjectName(QStringLiteral("btnAlphaShape"));

        horizontalLayout_2->addWidget(btnAlphaShape);


        gridLayout_2->addWidget(horizontalGroupBox, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox_2);

        dockScene->setWidget(dockWidgetContents);
        QtPlasmaClass->addDockWidget(static_cast<Qt::DockWidgetArea>(1), dockScene);
        dockDetail = new QDockWidget(QtPlasmaClass);
        dockDetail->setObjectName(QStringLiteral("dockDetail"));
        dockDetail->setFeatures(QDockWidget::DockWidgetFloatable|QDockWidget::DockWidgetMovable);
        dockWidgetContents_2 = new QWidget();
        dockWidgetContents_2->setObjectName(QStringLiteral("dockWidgetContents_2"));
        gridLayout_3 = new QGridLayout(dockWidgetContents_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        groupBox_3 = new QGroupBox(dockWidgetContents_2);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_4 = new QGridLayout(groupBox_3);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        tableWidget = new QTableWidget(groupBox_3);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));

        gridLayout_4->addWidget(tableWidget, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_3, 0, 0, 1, 1);

        dockDetail->setWidget(dockWidgetContents_2);
        QtPlasmaClass->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockDetail);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(actionOpen);
        menuView->addAction(actionWireframe);

        retranslateUi(QtPlasmaClass);

        QMetaObject::connectSlotsByName(QtPlasmaClass);
    } // setupUi

    void retranslateUi(QMainWindow *QtPlasmaClass)
    {
        QtPlasmaClass->setWindowTitle(QApplication::translate("QtPlasmaClass", "QtPlasma", 0));
        actionOpen->setText(QApplication::translate("QtPlasmaClass", "Open", 0));
        actionOpen->setShortcut(QApplication::translate("QtPlasmaClass", "Ctrl+O", 0));
        actionWireframe->setText(QApplication::translate("QtPlasmaClass", "Wireframe", 0));
        menuFile->setTitle(QApplication::translate("QtPlasmaClass", "File", 0));
        menuView->setTitle(QApplication::translate("QtPlasmaClass", "View", 0));
        dockScene->setWindowTitle(QApplication::translate("QtPlasmaClass", "Scene outliner", 0));
        groupBox->setTitle(QApplication::translate("QtPlasmaClass", "Data source", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeDataSource->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("QtPlasmaClass", "Type", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("QtPlasmaClass", "Name", 0));
        groupBox_2->setTitle(QApplication::translate("QtPlasmaClass", "Rendering context", 0));
        btnApply->setText(QApplication::translate("QtPlasmaClass", "Clear", 0));
        QTreeWidgetItem *___qtreewidgetitem1 = treeRenderContext->headerItem();
        ___qtreewidgetitem1->setText(1, QApplication::translate("QtPlasmaClass", "Type", 0));
        ___qtreewidgetitem1->setText(0, QApplication::translate("QtPlasmaClass", "Data", 0));
        btnVolume->setText(QApplication::translate("QtPlasmaClass", "Volume", 0));
        btnEField->setText(QApplication::translate("QtPlasmaClass", "E Field", 0));
        btnIsosurface->setText(QApplication::translate("QtPlasmaClass", "Iso-surface", 0));
        btnAlphaShape->setText(QApplication::translate("QtPlasmaClass", "Alpha-shape", 0));
        dockDetail->setWindowTitle(QApplication::translate("QtPlasmaClass", "Detail", 0));
        groupBox_3->setTitle(QApplication::translate("QtPlasmaClass", "Properties", 0));
    } // retranslateUi

};

namespace Ui {
    class QtPlasmaClass: public Ui_QtPlasmaClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTPLASMA_H
