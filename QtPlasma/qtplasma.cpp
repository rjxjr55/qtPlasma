#include "stdafx.h"
#include "qtplasma.h"

#include <QFileDialog>

#include "Vector.hpp"
#include "Importer.h"
#include "Scene.h"
#include "HDFSource.h"
#include "Volume.h"
#include "VectorField.h"
#include "Surface.h"
#include "CustomDialog.h"

#include "efield.cuh"

QtPlasma::QtPlasma(QWidget *parent)
	: QMainWindow(parent), m_importer(Importer::GetInstance()), m_scene(Scene::GetInstance())
{
	alpha = 64;

	ui.setupUi(this);
	connect(ui.actionOpen, SIGNAL(triggered()), this, SLOT(OnOpenMenuClicked()));
	connect(ui.actionWireframe, SIGNAL(toggled(bool)), this, SLOT(OnWireframeMenuClicked(bool)));
	connect(ui.btnVolume, SIGNAL(clicked()), this, SLOT(OnVolumeBtnClicked()));
	connect(ui.btnEField, SIGNAL(clicked()), this, SLOT(OnEFieldBtnClicked()));
	connect(ui.btnIsosurface, SIGNAL(clicked()), this, SLOT(OnIsosufaceBtnClicked()));
	connect(ui.btnAlphaShape, SIGNAL(clicked()), this, SLOT(OnAlphaShapeBtnClicked()));
	connect(ui.btnApply, SIGNAL(clicked()), this, SLOT(OnApplyBtnClicked()));

	m_importer.SetDataSourceTree(ui.treeDataSource);
	m_pContext = new QOpenGLContext();
}

QtPlasma::~QtPlasma()
{
	delete m_pContext;
}

void QtPlasma::OnOpenMenuClicked()
{
	QFileDialog::Options options = QFileDialog::DontResolveSymlinks;
#ifdef _DEBUG
	QString file = QFileDialog::getOpenFileName(this, tr("Open data"), "C:\\Repo\\QtPlasma7.5\\data", nullptr, nullptr, options);
#else
	QString file = QFileDialog::getOpenFileName(this, tr("Open data"), "C:\\", nullptr, nullptr, options);
#endif // _DEBUG

	if (!file.isEmpty()) {
		std::string path = file.toUtf8().toStdString();
		DataType type;
		std::string ext = path.substr(path.find_last_of(".") + 1);
		if (ext == "h5") {
			type = HDF;
		} else if (ext == "raw") {
			type = RAW;
		}

		m_importer.Load(path, type);
	}
}

void QtPlasma::OnEFieldBtnClicked()
{
// 	std::vector<DataSource*>& data = m_importer.GetDataSources();
// 	HDFSource* ion = (HDFSource*)data[0];
// 	HDFSource* electron = (HDFSource*)data[1];
// 
// 	Vector3i dimVectorField = { 11, 20, 20 };
// 	std::vector<Vector3f>* output = CalculateEField(ion->GetDimension()[0], ion->GetDimension()[1], ion->GetDimension()[2], ion->GetData(), electron->GetData(), dimVectorField);
// 	m_scene.AddDrawable(new VectorField(output, dimVectorField));
// 	ui.openGLWidget->updateGL();
}

void QtPlasma::OnIsosufaceBtnClicked()
{
	QInputDialog* inputDialog = new QInputDialog();
	inputDialog->setOptions(QInputDialog::NoButtons);

	bool ok;
	double isovalue = inputDialog->getDouble(NULL, "Input Iso-value",
		"iso-value:", 0, -2147483647, 2147483647, 1, &ok);

	if (ok) {
		std::vector<DataSource*>& data = m_importer.GetDataSources();
		HDFSource* charge = (HDFSource*)data[0];
		m_scene.AddDrawable(charge->ConstructSurface(isovalue * 1.0E26, 0.06));
		ui.openGLWidget->updateGL();
	}
}

void QtPlasma::OnAlphaShapeBtnClicked()
{
// 	QInputDialog* inputDialog = new QInputDialog();
// 	inputDialog->setOptions(QInputDialog::NoButtons);
// 
// 	bool ok;
// 	unsigned int y = inputDialog->getInt(NULL, "Input Y value",
// 		"Y:", 0, 0, 1000, 10, &ok);
// 	double min = inputDialog->getDouble(NULL, "Input min value",
// 		"Min:", 0, -2147483647, 2147483647, 1, &ok);
// 	double max = inputDialog->getDouble(NULL, "Input max value",
// 		"Max:", 0, -2147483647, 2147483647, 10, &ok);
// 	double alpha = inputDialog->getDouble(NULL, "Input alpha",
// 		"Alpha:", 0, -2147483647, 2147483647, 10, &ok);

	CustomDialog dialog("Input Alpha", this);
	int min = 8, max = 256, step = 1;
	dialog.addSlider("alpha", min, max, &alpha, step, this, SLOT(OnAlphaChanged(int)));
	dialog.exec();

// 	if (ok) {
// 		std::vector<DataSource*>& data = m_importer.GetDataSources();
// 		HDFSource* charge = (HDFSource*)data[0];
// 		m_scene.AddDrawable((IDrawable*)charge->CalculateAlphaShape(alpha));
// 		ui.openGLWidget->updateGL();
// 	}

	std::vector<DataSource*>& data = m_importer.GetDataSources();
	HDFSource* charge = (HDFSource*)data[0];
	m_scene.Empty();
	m_scene.AddDrawable((IDrawable*)charge->CalculateAlphaShape(alpha));
	ui.openGLWidget->updateGL();
}

void QtPlasma::OnApplyBtnClicked()
{
	m_scene.Empty();
	//m_importer.Empty();
	ui.openGLWidget->updateGL();
}

void QtPlasma::OnWireframeMenuClicked(bool checked)
{
	if (checked)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	ui.openGLWidget->updateGL();
}

void QtPlasma::OnVolumeBtnClicked()
{
	std::vector<DataSource*>& data = m_importer.GetDataSources();
	VolumeDataSource* charge = (VolumeDataSource*)data[0];
	m_scene.AddDrawable(charge->GetVolume(ivec3(255,128,0)));
}

void QtPlasma::OnAlphaChanged(int value)
{
	std::vector<DataSource*>& data = m_importer.GetDataSources();
	HDFSource* charge = (HDFSource*)data[0];
	m_scene.Empty();
	m_scene.AddDrawable((IDrawable*)charge->CalculateAlphaShape(value));
	ui.openGLWidget->updateGL();
}
