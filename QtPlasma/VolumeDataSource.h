#pragma once
#include "DataSource.h"
#include "Volume.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Alpha_shape_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

typedef CGAL::Alpha_shape_vertex_base_3<K>               Vb;
typedef CGAL::Alpha_shape_cell_base_3<K>                 Fb;
typedef CGAL::Triangulation_data_structure_3<Vb, Fb/*, CGAL::Parallel_tag*/>      Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds, CGAL::Fast_location>  Delaunay;
typedef CGAL::Alpha_shape_3<Delaunay>                    Alpha_shape_3;

typedef K::Point_3                                  Point;
typedef Alpha_shape_3::Alpha_iterator               Alpha_iterator;
typedef Alpha_shape_3::NT                           NT;

typedef K::FT FT;
typedef K::Point_2  Point2;
typedef K::Segment_2  Segment;
typedef CGAL::Alpha_shape_vertex_base_2<K> Vb2;
typedef CGAL::Alpha_shape_face_base_2<K>  Fb2;
typedef CGAL::Triangulation_data_structure_2<Vb2, Fb2> Tds2;
typedef CGAL::Delaunay_triangulation_2<K, Tds2> Triangulation_2;
typedef CGAL::Alpha_shape_2<Triangulation_2>  Alpha_shape_2;
typedef Alpha_shape_2::Alpha_shape_edges_iterator Alpha_shape_edges_iterator;

class Surface;
class Lines;

class VolumeDataSource :
	public DataSource
{
protected:
	unsigned int m_dims[3];
	float* m_data;
	Alpha_shape_3* as;

public:
	VolumeDataSource();
	~VolumeDataSource();

	unsigned int* GetDimension();
	float* GetData();

	Surface* ConstructSurface(float isoValue, float scale);
	Surface* CalculateAlphaShape(float alpha);
	Lines* CalculateAlphaShape2(float min, float max, float alpha, int slice);
	Volume<float>* GetVolume(ivec3);
	Volume<float>* GetVolume();

	float Sample(unsigned int x, unsigned int y, unsigned int z);

private:
	void _MarchCube(Surface* surface, unsigned int x, unsigned int y, unsigned int z, float isoValue, float fScale);
};

