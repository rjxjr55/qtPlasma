#pragma once
#include "VolumeDataSource.h"

class RawSource :
	public VolumeDataSource
{
private:
	std::string m_fileName;

public:
	RawSource(std::string file, int dimX, int dimY, int dimZ);
	virtual ~RawSource();

	virtual void Load(std::string file);
};

