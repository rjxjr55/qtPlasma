#include "stdafx.h"
#include "Scene.h"

#include "IDrawable.h"

Scene::~Scene()
{
	for (auto i = m_drawList.begin(); i != m_drawList.end(); i++) {
		delete *i;
	}
}

Scene& Scene::GetInstance()
{
	return instance;
}

void Scene::Update(float dt)
{
	for (auto i = m_drawList.begin(); i != m_drawList.end(); i++) {
		(*i)->Update(dt);
	}
}

void Scene::Draw(float dt)
{
	for (auto i = m_drawList.begin(); i != m_drawList.end(); i++) {
		(*i)->Draw(dt);
	}
}

void Scene::AddDrawable(IDrawable* item)
{
	m_drawList.push_back(item);
}

void Scene::Empty()
{
	m_drawList.clear();
}

Scene Scene::instance;